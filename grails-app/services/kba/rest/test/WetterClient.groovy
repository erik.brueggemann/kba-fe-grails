package kba.rest.test


import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client

@Client("http://api.openweathermap.org/data/2.5")
interface WetterClient {

    @Get("/weather?q={location}&units=metric&APPID=265e8fe45d9f3e5b5a6f8b972e03126f")
    String queryLocation(String location)
}