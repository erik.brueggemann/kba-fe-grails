package kba.rest.test

class Book {

    Long id
    String titel
    String autor
    Double preis
    Integer seitenanzahl

    static constraints = {
        titel blank:false
        autor blank:false
        preis blank:false
        seitenanzahl blank:false
    }
}
